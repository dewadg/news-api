# News API

A REST API equipped with ElasticSearch, RabbitMQ (for learning purpose)

# Running Development

Add `.env`:
```
APP_PORT=8000

DB_HOST=localhost
DB_PORT=3309
DB_DATABASE=dev_news_api
DB_USER=root
DB_PASSWORD=admin

ES_HOST=localhost
ES_PORT=9200

RABBITMQ_HOST=localhost
RABBITMQ_PORT=5672
RABBITMQ_USER=admin
RABBITMQ_PASSWORD=admin
```

Start required services:
```
docker-compose -f docker-compose.dev.yml up -d
```

Run API
```
go build
./news-api migrate
./news-api serve
```

# Testing
Make sure all services already started.
```
go test -v github.com/dewadg/news-api/repository
go test -v github.com/dewadg/news-api/service
go test -v github.com/dewadg/news-api/handler
```

# API
### `POST /news`
Request:
```
{
    "author": "dewadg",
    "body": "Lorem ipsum dolor"
}
```
Response:
```
{
  "message": "News successfully created"
}
```

### `GET /news` or `GET /news?page=1` *results are cached for 5seconds*
Response:
```
[
  {
    "id": 6,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:03:11"
  },
  {
    "id": 5,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:03:10"
  },
  {
    "id": 4,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:03:09"
  },
  {
    "id": 2,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:03:08"
  },
  {
    "id": 3,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:03:08"
  },
  {
    "id": 1,
    "author": "Degea",
    "body": "Aaaa",
    "created": "2019-05-20 11:02:08"
  }
]
```