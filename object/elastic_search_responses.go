package object

// ElasticSearchSearchResult represents search response from ES
type ElasticSearchSearchResult struct {
	Hits ElasticSearchItemList `json:"hits"`
}

// ElasticSearchItemList represents hit results from ES
type ElasticSearchItemList struct {
	Items []ElasticSearchItem `json:"hits"`
}

// ElasticSearchItem represents hit results item from ES
type ElasticSearchItem struct {
	Index  string                 `json:"_index"`
	Type   string                 `json:"_type"`
	ID     string                 `json:"_id"`
	Score  float32                `json:"_score"`
	Source map[string]interface{} `json:"_source"`
}
