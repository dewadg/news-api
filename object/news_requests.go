package object

import (
	"errors"
	"net/http"
)

// StoreNewsRequest represents request payload
// for creating new News
type StoreNewsRequest struct {
	Author string `json:"author"`
	Body   string `json:"body"`
}

// Bind checks the request
func (req *StoreNewsRequest) Bind(r *http.Request) error {
	if req.Author == "" {
		return errors.New("`author` is required")
	}
	if req.Body == "" {
		return errors.New("`body` is required")
	}
	return nil
}
