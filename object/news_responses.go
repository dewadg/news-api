package object

import (
	"net/http"

	"github.com/go-chi/render"
)

// NewsResponse represents response of News
type NewsResponse struct {
	ID      uint   `json:"id"`
	Author  string `json:"author"`
	Body    string `json:"body"`
	Created string `json:"created"`
}

// NewsCreatedResponse represents response when news created
type NewsCreatedResponse struct {
	Message string `json:"message"`
}

// Render pre-processes the response before sent
func (res *NewsResponse) Render(w http.ResponseWriter, r *http.Request) error {
	return nil
}

// Render pre-processes the response before sent
func (res *NewsCreatedResponse) Render(w http.ResponseWriter, r *http.Request) error {
	render.Status(r, http.StatusCreated)
	return nil
}
