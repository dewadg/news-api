package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/dewadg/news-api/db"
	"github.com/dewadg/news-api/handler"
	"github.com/dewadg/news-api/model"
	"github.com/dewadg/news-api/object"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	cache "github.com/victorspringer/http-cache"
	"github.com/victorspringer/http-cache/adapter/memory"
)

func createCacheClient() (*cache.Client, error) {
	memcached, err := memory.NewAdapter(
		memory.AdapterWithAlgorithm(memory.LRU),
		memory.AdapterWithCapacity(10000000),
	)
	if err != nil {
		return nil, err
	}

	cacheClient, err := cache.NewClient(
		cache.ClientWithAdapter(memcached),
		cache.ClientWithTTL(5*time.Second),
		cache.ClientWithRefreshKey("opn"),
	)
	if err != nil {
		return nil, err
	}
	return cacheClient, nil
}

func createRouter() chi.Router {
	r := chi.NewRouter()

	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Get("/", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		payload := map[string]interface{}{
			"name": "News API",
		}
		res, _ := json.Marshal(payload)
		w.Write(res)
	}))

	cacheClient, err := createCacheClient()
	if err != nil {
		log.Fatal(err)
	}

	newsHandler := handler.NewNewsHandler(newsService, elasticSearchService, queueService)

	r.Mount("/news", newsHandler.GetRoutes(cacheClient))

	return r
}

func serveHTTP() {
	router := createRouter()
	port := os.Getenv("APP_PORT")
	if port == "" {
		port = "8000"
	}

	fmt.Printf("App running on port %s\n", port)
	http.ListenAndServe(fmt.Sprintf(":%s", port), router)
}

func runMigration() {
	fmt.Println("Running migrations...")
	db.Migrate()
	fmt.Println("Migrated successfully")
}

func runQueueListener() {
	msgs, err := queueService.Consume()
	if err != nil {
		log.Fatal(err)
	}

	forever := make(chan bool)
	go func() {
		for d := range msgs {
			payload := object.StoreNewsRequest{}
			err := json.Unmarshal(d.Body, &payload)
			if err != nil {
				log.Fatal(err)
			}

			news, err := createNews(payload)
			if err != nil {
				log.Fatal(err)
			}

			fmt.Println("New news has been created")
			fmt.Println(news)
		}
	}()

	fmt.Println("Waiting for messages")
	<-forever
}

func createNews(payload object.StoreNewsRequest) (model.News, error) {
	news, err := newsService.Create(payload.Author, payload.Body)
	if err != nil {
		return model.News{}, err
	}

	err = elasticSearchService.AddItem("news", fmt.Sprintf("%d", news.ID), map[string]interface{}{
		"id":      news.ID,
		"created": news.Created.Format("2006-01-02 15:04:05"),
	})
	if err != nil {
		return model.News{}, err
	}
	return news, nil
}
