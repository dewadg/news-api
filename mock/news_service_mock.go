package mock

import (
	"github.com/dewadg/news-api/model"
	"github.com/stretchr/testify/mock"
)

// NewsServiceMock mocks NewsService
type NewsServiceMock struct {
	mock.Mock
}

// Create mocks Create
func (s *NewsServiceMock) Create(author, body string) (model.News, error) {
	args := s.Called(author, body)

	return args.Get(0).(model.News), args.Error(1)
}

// Find mocks Find
func (s *NewsServiceMock) Find(id uint) (model.News, error) {
	args := s.Called(id)

	return args.Get(0).(model.News), args.Error(1)
}
