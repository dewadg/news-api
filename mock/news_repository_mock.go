package mock

import (
	"github.com/dewadg/news-api/model"
	"github.com/stretchr/testify/mock"
)

// NewsRepositoryMock mocks NewsRepository
type NewsRepositoryMock struct {
	mock.Mock
}

// Save mocks Save
func (r *NewsRepositoryMock) Save(data *model.News) error {
	args := r.Called(data)

	return args.Error(0)
}

// Find mocks Find
func (r *NewsRepositoryMock) Find(id uint) model.News {
	args := r.Called(id)

	return args.Get(0).(model.News)
}
