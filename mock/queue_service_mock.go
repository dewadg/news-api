package mock

import (
	"github.com/streadway/amqp"
	"github.com/stretchr/testify/mock"
)

// QueueServiceMock mocks QueueService
type QueueServiceMock struct {
	mock.Mock
}

// Publish mocks Publish
func (s *QueueServiceMock) Publish(payload interface{}) error {
	args := s.Called(payload)

	return args.Error(0)
}

// Consume mocks Consume
func (s *QueueServiceMock) Consume() (<-chan amqp.Delivery, error) {
	args := s.Called()

	return args.Get(0).(<-chan amqp.Delivery), args.Error(1)
}
