package mock

import (
	"github.com/dewadg/news-api/object"
	"github.com/stretchr/testify/mock"
)

// ElasticSearchServiceMock mocks ElasticSearchService
type ElasticSearchServiceMock struct {
	mock.Mock
}

// AddIndex mocks AddIndex
func (s *ElasticSearchServiceMock) AddIndex(index string, params map[string]interface{}) error {
	args := s.Called(index, params)

	return args.Error(0)
}

// IndexExists mocks IndexExists
func (s *ElasticSearchServiceMock) IndexExists(index string) (bool, error) {
	args := s.Called(index)

	return args.Bool(0), args.Error(1)
}

// AddItem mocks AddItem
func (s *ElasticSearchServiceMock) AddItem(index, ID string, item map[string]interface{}) error {
	args := s.Called(index, ID, item)

	return args.Error(0)
}

// GetItems mocks GetItems
func (s *ElasticSearchServiceMock) GetItems(index string, count, from int, params map[string]interface{}) ([]object.ElasticSearchItem, error) {
	args := s.Called(index, count, from, params)

	return args.Get(0).([]object.ElasticSearchItem), args.Error(1)
}

// Clear mocks Clear
func (s *ElasticSearchServiceMock) Clear() error {
	args := s.Called()

	return args.Error(0)
}
