FROM golang AS build

WORKDIR /go/src/github.com/dewadg/news-api

ADD . .

RUN go get -v ./...
RUN CGO_ENABLED=0 go build

FROM alpine

WORKDIR /usr/local/bin

COPY --from=build /go/src/github.com/dewadg/news-api/news-api ./news-api
RUN chmod +x ./news-api

CMD ["news-api", "serve"]
