package main

import (
	"log"
	"os"
	"strconv"

	"github.com/dewadg/news-api/db"
	"github.com/dewadg/news-api/repository"
	"github.com/dewadg/news-api/service"
)

var newsService *service.NewsService
var elasticSearchService *service.ElasticSearchService
var queueService *service.QueueService

func initServices() {
	var err error

	esHost := os.Getenv("ES_HOST")
	esPort, _ := strconv.Atoi(os.Getenv("ES_PORT"))
	rabbitMQHost := os.Getenv("RABBITMQ_HOST")
	rabbitMQPort, _ := strconv.Atoi(os.Getenv("RABBITMQ_PORT"))
	rabbitMQUser := os.Getenv("RABBITMQ_USER")
	rabbitMQPassword := os.Getenv("RABBITMQ_PASSWORD")
	rabbitMQQueue := "news"

	dbConn := db.Get()

	newsService = service.NewNewsService(repository.NewNewsRepository(dbConn))
	elasticSearchService = service.NewElasticSearchService(esHost, esPort)
	queueService, err = service.NewQueueService(rabbitMQHost, rabbitMQPort, rabbitMQUser, rabbitMQPassword, rabbitMQQueue)

	if err != nil {
		log.Fatal(err)
	}
}

func initElasticSearch() {
	var err error

	index := "news"
	indexExists, err := elasticSearchService.IndexExists(index)
	if err != nil {
		log.Fatal(err)
	}

	if !indexExists {
		err = elasticSearchService.AddIndex(index, map[string]interface{}{
			"mappings": map[string]interface{}{
				"properties": map[string]interface{}{
					"id": map[string]interface{}{
						"type": "integer",
					},
					"created": map[string]interface{}{
						"type":   "date",
						"format": "yyyy-MM-dd HH:mm:ss",
					},
				},
			},
		})
		if err != nil {
			log.Fatal(err)
		}
	}
}
