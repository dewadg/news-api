package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/dewadg/news-api/object"
)

// ElasticSearchService represents service
// for communicating with ES
type ElasticSearchService struct {
	host string
	port int
	http *http.Client
}

// IndexExists checks whether index exists on ES
func (s *ElasticSearchService) IndexExists(index string) (bool, error) {
	url := fmt.Sprintf("http://%s:%d/%s", s.host, s.port, index)
	req, err := http.NewRequest(http.MethodHead, url, nil)
	if err != nil {
		return false, err
	}

	res, err := s.http.Do(req)
	if err != nil {
		return false, err
	}

	if res.StatusCode != http.StatusOK {
		return false, nil
	}
	return true, nil
}

// AddIndex adds index to ES
func (s *ElasticSearchService) AddIndex(name string, params map[string]interface{}) error {
	url := fmt.Sprintf("http://%s:%d/%s", s.host, s.port, name)
	payload, err := json.Marshal(params)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}

	res, err := s.http.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("Failed to create index %s", name)
	}
	return nil
}

// AddItem stores item to ES
func (s *ElasticSearchService) AddItem(index, ID string, item map[string]interface{}) error {
	url := fmt.Sprintf("http://%s:%d/%s/_doc/%s", s.host, s.port, index, ID)
	payload, err := json.Marshal(item)
	if err != nil {
		return err
	}

	req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return err
	}

	res, err := s.http.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusCreated {
		return fmt.Errorf("Failed to add item to index %s", index)
	}
	return nil
}

// GetItems get items of given index from ES
func (s *ElasticSearchService) GetItems(index string, count, from int, params map[string]interface{}) ([]object.ElasticSearchItem, error) {
	url := fmt.Sprintf("http://%s:%d/%s/_search?size=%d&from=%d", s.host, s.port, index, count, from)
	payload, err := json.Marshal(params)
	if err != nil {
		return []object.ElasticSearchItem{}, err
	}

	req, err := http.NewRequest(http.MethodGet, url, bytes.NewBuffer(payload))
	req.Header.Set("Content-Type", "application/json")
	if err != nil {
		return []object.ElasticSearchItem{}, err
	}

	res, err := s.http.Do(req)
	if err != nil {
		return []object.ElasticSearchItem{}, err
	}

	var data object.ElasticSearchSearchResult
	err = json.NewDecoder(res.Body).Decode(&data)
	if err != nil {
		return []object.ElasticSearchItem{}, err
	}

	return data.Hits.Items, nil
}

// Clear removes all index
func (s *ElasticSearchService) Clear() error {
	url := fmt.Sprintf("http://%s:%d/_all", s.host, s.port)
	req, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		return err
	}

	res, err := s.http.Do(req)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		return errors.New("Failed clear indexes")
	}
	return nil
}
