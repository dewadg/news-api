package service

import (
	"fmt"
	"os"
	"strconv"
	"testing"
	"time"
)

func TestElasticSearchServiceIndexExists(t *testing.T) {
	host := os.Getenv("ES_HOST")
	port, _ := strconv.Atoi(os.Getenv("ES_PORT"))
	service := NewElasticSearchService(host, port)
	service.Clear()

	expected := false

	actual, err := service.IndexExists(_testFaker.Lorem().Word())
	if err != nil {
		t.Error("ElasticSearchService.IndexExists failed with error")
		t.Errorf(err.Error())
	}
	if actual != expected {
		t.Error("ElasticSearchService.IndexExists failed to retrieve expected data")
	}
}

func TestElasticSearchAddIndex(t *testing.T) {
	host := os.Getenv("ES_HOST")
	port, _ := strconv.Atoi(os.Getenv("ES_PORT"))
	service := NewElasticSearchService(host, port)
	service.Clear()

	payload := _testFaker.Lorem().Word()

	err := service.AddIndex(payload, map[string]interface{}{
		"mappings": map[string]interface{}{
			"properties": map[string]interface{}{
				"id": map[string]interface{}{
					"type": "integer",
				},
				"created": map[string]interface{}{
					"type":   "date",
					"format": "yyyy-MM-dd HH:mm:ss",
				},
			},
		},
	})
	if err != nil {
		t.Error("ElasticSearchService.AddIndex failed with error")
		t.Errorf(err.Error())
	}
}

func TestElasticSearchAddItem(t *testing.T) {
	host := os.Getenv("ES_HOST")
	port, _ := strconv.Atoi(os.Getenv("ES_PORT"))
	service := NewElasticSearchService(host, port)
	service.Clear()

	index := _testFaker.Lorem().Word()

	service.AddIndex(index, map[string]interface{}{
		"mappings": map[string]interface{}{
			"properties": map[string]interface{}{
				"id": map[string]interface{}{
					"type": "integer",
				},
				"created": map[string]interface{}{
					"type":   "date",
					"format": "yyyy-MM-dd HH:mm:ss",
				},
			},
		},
	})

	payloadID := _testFaker.RandomNumber(2)
	payload := map[string]interface{}{
		"id":      payloadID,
		"created": time.Now().Format("2006-01-02 15:04:05"),
	}

	err := service.AddItem(index, fmt.Sprintf("%d", payloadID), payload)
	if err != nil {
		t.Error("ElasticSearchService.AddItem failed with error")
		t.Errorf(err.Error())
	}
}

func TestElasticSearchGetItems(t *testing.T) {
	host := os.Getenv("ES_HOST")
	port, _ := strconv.Atoi(os.Getenv("ES_PORT"))
	service := NewElasticSearchService(host, port)
	service.Clear()

	index := _testFaker.Lorem().Word()

	err := service.AddIndex(index, map[string]interface{}{
		"mappings": map[string]interface{}{
			"properties": map[string]interface{}{
				"id": map[string]interface{}{
					"type": "integer",
				},
				"created": map[string]interface{}{
					"type":   "date",
					"format": "yyyy-MM-dd HH:mm:ss",
				},
			},
		},
	})
	if err != nil {
		t.Error("ElasticSearchService.GetItems failed with error")
		t.Errorf(err.Error())
	}

	expected := []map[string]interface{}{
		map[string]interface{}{
			"id":      1,
			"created": time.Now().Format("2006-01-02 15:04:05"),
		},
		map[string]interface{}{
			"id":      2,
			"created": time.Now().Format("2006-01-02 15:04:05"),
		},
	}

	for i, item := range expected {
		err := service.AddItem(index, fmt.Sprintf("%d", i), item)
		if err != nil {
			t.Error("ElasticSearchService.GetItems failed with error")
			t.Errorf(err.Error())
		}

		// Delay it a bit
		time.Sleep(1000 * time.Millisecond)
	}

	items, err := service.GetItems(index, 10, 0, map[string]interface{}{
		"sort": []map[string]interface{}{
			map[string]interface{}{
				"created": map[string]interface{}{
					"order": "desc",
				},
			},
		},
	})
	if err != nil {
		t.Error("ElasticSearchService.GetItems failed with error")
		t.Errorf(err.Error())
	}

	if len(items) != len(expected) {
		t.Error("ElasticSearchService.GetItems failed to retrieve expected data")
	}

	actual := make([]map[string]interface{}, 0)
	for _, item := range items {
		actual = append(actual, item.Source)
	}

	for i, item := range expected {
		expectedID := item["id"]
		actualID := int(actual[i]["id"].(float64))
		expectedCreated := item["content"]
		actualCreated := actual[i]["content"]

		if expectedID != actualID || expectedCreated != actualCreated {
			t.Error("ElasticSearchService.GetItems failed to retrieve expected data")
		}
	}
}
