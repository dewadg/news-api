package service

import (
	"fmt"

	"github.com/dewadg/news-api/model"
	"github.com/dewadg/news-api/repository"
)

// NewsService represents service layer
// for News
type NewsService struct {
	repository repository.NewsRepositoryContract
}

// Create saves new News
func (s *NewsService) Create(author, body string) (model.News, error) {
	payload := model.News{
		Author: author,
		Body:   body,
	}

	err := s.repository.Save(&payload)
	if err != nil {
		return model.News{}, err
	}
	return payload, nil
}

// Find returns News by ID
func (s *NewsService) Find(id uint) (model.News, error) {
	news := s.repository.Find(id)
	if news.ID == 0 {
		return model.News{}, fmt.Errorf(fmt.Sprintf("News with ID %d not found", id))
	}
	return news, nil
}
