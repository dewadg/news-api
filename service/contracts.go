package service

import (
	"github.com/dewadg/news-api/model"
	"github.com/dewadg/news-api/object"
	"github.com/streadway/amqp"
)

// NewsServiceContract represents interface
// for NewsService
type NewsServiceContract interface {
	Create(author, body string) (model.News, error)
	Find(id uint) (model.News, error)
}

// ElasticSearchServiceContract represents interface
// for ElasticSearchService
type ElasticSearchServiceContract interface {
	AddIndex(index string, params map[string]interface{}) error
	IndexExists(index string) (bool, error)
	AddItem(index, ID string, item map[string]interface{}) error
	GetItems(index string, count, from int, params map[string]interface{}) ([]object.ElasticSearchItem, error)
	Clear() error
}

// QueueServiceContract represents interface
// for QueueService
type QueueServiceContract interface {
	Publish(payload interface{}) error
	Consume() (<-chan amqp.Delivery, error)
}
