package service

import (
	"fmt"
	"net/http"

	"github.com/dewadg/news-api/repository"
	"github.com/streadway/amqp"
)

// NewNewsService returns new instance of NewsService
func NewNewsService(repository repository.NewsRepositoryContract) *NewsService {
	return &NewsService{
		repository: repository,
	}
}

// NewElasticSearchService returns new instance of ElasticSearchService
func NewElasticSearchService(host string, port int) *ElasticSearchService {
	return &ElasticSearchService{
		host: host,
		port: port,
		http: &http.Client{},
	}
}

// NewQueueService returns new instance of QueueService
func NewQueueService(host string, port int, user string, password string, queue string) (*QueueService, error) {
	amqpConn, err := amqp.Dial(fmt.Sprintf(
		"amqp://%s:%s@%s:%d/",
		user,
		password,
		host,
		port),
	)

	if err != nil {
		return &QueueService{}, err
	}
	return &QueueService{
		queue: queue,
		amqp:  amqpConn,
	}, nil
}
