package service

import (
	"encoding/json"

	"github.com/streadway/amqp"
)

// QueueService represents service for
// communicating with RabbitMQ
type QueueService struct {
	queue string
	amqp  *amqp.Connection
}

func (s *QueueService) createChannel() (*amqp.Channel, error) {
	return s.amqp.Channel()
}

func (s *QueueService) createQueue(channel *amqp.Channel) (amqp.Queue, error) {
	return channel.QueueDeclare(
		s.queue,
		true,
		false,
		false,
		false,
		nil,
	)
}

// Publish sends payload to queue
func (s *QueueService) Publish(payload interface{}) error {
	channel, err := s.createChannel()
	if err != nil {
		return err
	}
	defer channel.Close()

	queue, err := s.createQueue(channel)
	if err != nil {
		return err
	}

	message, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	return channel.Publish(
		"",
		queue.Name,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        message,
		},
	)
}

// Consume gets data from queue
func (s *QueueService) Consume() (<-chan amqp.Delivery, error) {
	channel, err := s.createChannel()
	if err != nil {
		return nil, err
	}

	queue, err := s.createQueue(channel)
	if err != nil {
		return nil, err
	}

	return channel.Consume(
		queue.Name,
		"",
		true,
		false,
		false,
		false,
		nil,
	)
}
