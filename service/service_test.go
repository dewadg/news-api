package service

import (
	"fmt"
	"testing"

	"github.com/jaswdr/faker"
	"github.com/joho/godotenv"
)

var _testFaker faker.Faker

func TestMain(m *testing.M) {
	err := godotenv.Load("../.env.test")
	if err != nil {
		fmt.Println("No .env.test file was specified")
	}

	_testFaker = faker.New()

	m.Run()
}
