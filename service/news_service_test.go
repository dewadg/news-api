package service

import (
	"reflect"
	"testing"

	localMock "github.com/dewadg/news-api/mock"
	"github.com/dewadg/news-api/model"
)

func TestNewsServiceCreate(t *testing.T) {
	newsRepository := &localMock.NewsRepositoryMock{}
	newsService := NewNewsService(newsRepository)

	payloadAuthor := _testFaker.Person().Name()
	payloadBody := _testFaker.Lorem().Word()
	payload := model.News{
		Author: payloadAuthor,
		Body:   payloadBody,
	}
	newsRepository.On("Save", &payload).Return(nil)

	expected := model.News{
		ID:     0,
		Author: payloadAuthor,
		Body:   payloadBody,
	}

	actual, err := newsService.Create(payloadAuthor, payloadBody)
	if err != nil {
		t.Error("NewsService.Create failed with error")
		t.Errorf(err.Error())
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("NewsService.Create failed to save data")
	}
}

func TestNewsServiceFind(t *testing.T) {
	newsRepository := &localMock.NewsRepositoryMock{}
	newsService := NewNewsService(newsRepository)

	payload := uint(1)
	expected := model.News{
		ID:     payload,
		Author: _testFaker.Person().Name(),
		Body:   _testFaker.Lorem().Word(),
	}
	newsRepository.On("Find", payload).Return(expected)

	actual, err := newsService.Find(payload)
	if err != nil {
		t.Error("NewsService.Find failed with error")
		t.Errorf(err.Error())
	}
	if !reflect.DeepEqual(actual, expected) {
		t.Errorf("NewsService.Find failed to retrieve expected data")
	}
}
