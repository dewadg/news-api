package handler

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	localMock "github.com/dewadg/news-api/mock"
	"github.com/dewadg/news-api/model"
	"github.com/dewadg/news-api/object"
	"github.com/stretchr/testify/assert"
)

func TestNewsHandlerStore(t *testing.T) {
	newsServiceMock := &localMock.NewsServiceMock{}
	elasticSearchServiceMock := &localMock.ElasticSearchServiceMock{}
	queueServiceMock := &localMock.QueueServiceMock{}
	newsHandler := &NewsHandler{
		newsService:          newsServiceMock,
		elasticSearchService: elasticSearchServiceMock,
		queueService:         queueServiceMock,
	}

	payloadAuthor := "Author 1"
	payloadBody := "Lorem ipsum dolor 1"

	newsServiceExpectedReturn1 := model.News{
		ID:      1,
		Author:  payloadAuthor,
		Body:    payloadBody,
		Created: time.Now(),
	}
	newsServiceMock.On("Create", payloadAuthor, payloadBody).Return(newsServiceExpectedReturn1, nil)

	payloadQueue := object.StoreNewsRequest{
		Author: payloadAuthor,
		Body:   payloadBody,
	}
	queueServiceMock.On("Publish", payloadQueue).Return(nil)

	payload := map[string]string{
		"author": payloadAuthor,
		"body":   payloadBody,
	}
	reqData, _ := json.Marshal(payload)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("POST", "/", bytes.NewBuffer(reqData))
	r.Header.Set("Content-Type", "application/json")

	http.HandlerFunc(newsHandler.Store).ServeHTTP(w, r)

	if status := w.Code; status != http.StatusCreated {
		t.Errorf("NewsHandler.Store() failed with status code %d", status)
	}

	expectedResponse := string(`
		{
			"message": "News successfully created"
		}
	`)

	assert.JSONEq(t, expectedResponse, w.Body.String())
}

func TestNewsHandlerIndex(t *testing.T) {
	newsServiceMock := &localMock.NewsServiceMock{}
	elasticSearchServiceMock := &localMock.ElasticSearchServiceMock{}
	queueServiceMock := &localMock.QueueServiceMock{}
	newsHandler := &NewsHandler{
		newsService:          newsServiceMock,
		elasticSearchService: elasticSearchServiceMock,
		queueService:         queueServiceMock,
	}

	elasticSearchServicePayloadQueue := "news"
	elasticSearchServicePayloadQueryCount := 10
	elasticSearchServicePayloadQueryPage := 0
	elasticSearchServicePayloadParams := map[string]interface{}{
		"sort": []map[string]interface{}{
			map[string]interface{}{
				"created": map[string]interface{}{
					"order": "desc",
				},
			},
		},
	}
	elasticSearchServiceExpectedReturn := []object.ElasticSearchItem{
		object.ElasticSearchItem{
			Source: map[string]interface{}{
				"id":      float64(1),
				"created": "2019-05-20 01:00:00",
			},
		},
		object.ElasticSearchItem{
			Source: map[string]interface{}{
				"id":      float64(2),
				"created": "2019-05-20 02:00:00",
			},
		},
		object.ElasticSearchItem{
			Source: map[string]interface{}{
				"id":      float64(3),
				"created": "2019-05-20 03:00:00",
			},
		},
	}
	elasticSearchServiceMock.On(
		"GetItems",
		elasticSearchServicePayloadQueue,
		elasticSearchServicePayloadQueryCount,
		elasticSearchServicePayloadQueryPage,
		elasticSearchServicePayloadParams,
	).Return(elasticSearchServiceExpectedReturn, nil)

	newsServiceExpectedReturnTime1, _ := time.Parse("2006-01-02 15:04:05", "2019-05-20 01:00:00")
	newsServiceExpectedReturnTime2, _ := time.Parse("2006-01-02 15:04:05", "2019-05-20 02:00:00")
	newsServiceExpectedReturnTime3, _ := time.Parse("2006-01-02 15:04:05", "2019-05-20 03:00:00")
	newsServiceExpectedReturns := []model.News{
		model.News{
			ID:      1,
			Author:  "Author 1",
			Body:    "Lorem ipsum dolor 1",
			Created: newsServiceExpectedReturnTime1,
		},
		model.News{
			ID:      2,
			Author:  "Author 2",
			Body:    "Lorem ipsum dolor 2",
			Created: newsServiceExpectedReturnTime2,
		},
		model.News{
			ID:      3,
			Author:  "Author 3",
			Body:    "Lorem ipsum dolor 3",
			Created: newsServiceExpectedReturnTime3,
		},
	}
	newsServiceMock.On("Find", newsServiceExpectedReturns[0].ID).Return(newsServiceExpectedReturns[0], nil)
	newsServiceMock.On("Find", newsServiceExpectedReturns[1].ID).Return(newsServiceExpectedReturns[1], nil)
	newsServiceMock.On("Find", newsServiceExpectedReturns[2].ID).Return(newsServiceExpectedReturns[2], nil)

	w := httptest.NewRecorder()
	r := httptest.NewRequest("GET", "/", nil)
	r.Header.Set("Content-Type", "application/json")

	http.HandlerFunc(newsHandler.Index).ServeHTTP(w, r)

	if status := w.Code; status != http.StatusOK {
		t.Errorf("NewsHandler.Index() failed with status code %d", status)
	}

	// Response should be sorted by `created` descending
	expectedResponse := string(`
		[
			{
				"id": 3,
				"author": "Author 3",
				"body": "Lorem ipsum dolor 3",
				"created": "2019-05-20 03:00:00"
			},
			{
				"id": 2,
				"author": "Author 2",
				"body": "Lorem ipsum dolor 2",
				"created": "2019-05-20 02:00:00"
			},
			{
				"id": 1,
				"author": "Author 1",
				"body": "Lorem ipsum dolor 1",
				"created": "2019-05-20 01:00:00"
			}
		]
	`)

	assert.JSONEq(t, expectedResponse, w.Body.String())
}
