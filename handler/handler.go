package handler

import (
	"github.com/dewadg/news-api/service"
)

// NewNewsHandler returns new instance of NewsHandler
func NewNewsHandler(
	newsService service.NewsServiceContract,
	elasticSearchService service.ElasticSearchServiceContract,
	queueService *service.QueueService,
) *NewsHandler {
	return &NewsHandler{
		newsService:          newsService,
		elasticSearchService: elasticSearchService,
		queueService:         queueService,
	}
}
