package handler

import (
	"net/http"
	"sort"
	"strconv"
	"sync"

	"github.com/dewadg/news-api/model"
	"github.com/dewadg/news-api/object"
	"github.com/dewadg/news-api/service"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	cache "github.com/victorspringer/http-cache"
)

// NewsHandler represents set of HTTP handler
// for News
type NewsHandler struct {
	newsService          service.NewsServiceContract
	elasticSearchService service.ElasticSearchServiceContract
	queueService         service.QueueServiceContract
}

// GetRoutes returns this handler's routes
func (h *NewsHandler) GetRoutes(cacheClient *cache.Client) chi.Router {
	r := chi.NewRouter()

	r.Post("/", h.Store)

	// Use cache if only client is given
	if cacheClient != nil {
		r.Route("/", func(subRouter chi.Router) {
			subRouter.Use(cacheClient.Middleware)
			subRouter.Get("/", h.Index)
		})
	} else {
		r.Get("/", h.Index)
	}

	return r
}

// Index returns list of available news
func (h *NewsHandler) Index(w http.ResponseWriter, r *http.Request) {
	queryCount := 10
	queryPage := 0
	if r.URL.Query().Get("page") != "" {
		queryPage, _ = strconv.Atoi(r.URL.Query().Get("page"))
		if queryPage == 1 {
			queryPage = 0
		} else {
			queryPage = ((queryPage - 1) * queryCount) + 1
		}
	}

	params := map[string]interface{}{
		"sort": []map[string]interface{}{
			map[string]interface{}{
				"created": map[string]interface{}{
					"order": "desc",
				},
			},
		},
	}
	data, err := h.elasticSearchService.GetItems("news", queryCount, queryPage, params)
	if err != nil {
		render.Render(w, r, createInternalServerErrorResponse(err.Error()))
		return
	}

	newsChan := make(chan model.News, len(data))
	var wg sync.WaitGroup

	for _, item := range data {
		wg.Add(1)
		newsID := uint(item.Source["id"].(float64))

		go func(ID uint, channel chan model.News, waitGroup *sync.WaitGroup) {
			news, err := h.newsService.Find(ID)
			if err != nil {
				render.Render(w, r, createInternalServerErrorResponse(err.Error()))
				return
			}

			defer waitGroup.Done()
			channel <- news
		}(newsID, newsChan, &wg)
	}
	wg.Wait()
	close(newsChan)

	newsList := make([]model.News, 0)
	for item := range newsChan {
		newsList = append(newsList, item)
	}
	sort.Slice(newsList, func(i, j int) bool {
		return newsList[j].Created.Before(newsList[i].Created)
	})

	newsListResponse := make([]render.Renderer, 0)
	for _, item := range newsList {
		newsListResponse = append(newsListResponse, &object.NewsResponse{
			ID:      item.ID,
			Author:  item.Author,
			Body:    item.Body,
			Created: item.Created.Format("2006-01-02 15:04:05"),
		})
	}

	render.RenderList(w, r, newsListResponse)
}

// Store handles storing of News
func (h *NewsHandler) Store(w http.ResponseWriter, r *http.Request) {
	req := object.StoreNewsRequest{}
	if err := render.Bind(r, &req); err != nil {
		if err.Error() == "EOF" {
			render.Render(w, r, createUnprocessableEntityResponse("`author`, `body` fields are required"))
		} else {
			render.Render(w, r, createUnprocessableEntityResponse(err.Error()))
		}
		return
	}

	err := h.queueService.Publish(req)
	if err != nil {
		render.Render(w, r, createInternalServerErrorResponse(err.Error()))
		return
	}

	render.Render(w, r, &object.NewsCreatedResponse{
		Message: "News successfully created",
	})
}
