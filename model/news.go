package model

import "time"

// News is model for news
type News struct {
	ID      uint
	Author  string
	Body    string
	Created time.Time
}
