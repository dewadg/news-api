package repository

import (
	"time"

	"github.com/dewadg/news-api/model"
	"github.com/jinzhu/gorm"
)

// NewsRepository represents repository
// for News
type NewsRepository struct {
	db *gorm.DB
}

// Save stores new News to DB
func (r *NewsRepository) Save(data *model.News) error {
	data.Created = time.Now()

	return r.db.Save(data).Error
}

// Find returns News by ID
func (r *NewsRepository) Find(id uint) model.News {
	data := model.News{}

	r.db.Where("id = ?", id).Find(&data)

	return data
}
