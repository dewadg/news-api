package repository

import (
	"testing"
	"time"

	"github.com/dewadg/news-api/db"
	"github.com/dewadg/news-api/model"
)

func TestNewsRepositorySave(t *testing.T) {
	db.Reset()

	newsRepository := NewNewsRepository(_testDB)

	expected := model.News{
		Author:  _testFaker.Person().Name(),
		Body:    _testFaker.Lorem().Word(),
		Created: time.Now(),
	}

	err := newsRepository.Save(&expected)
	if err != nil {
		t.Error("NewsRepository.Save failed with error")
		t.Errorf(err.Error())
	}
}

func TestNewsRepositoryFind(t *testing.T) {
	db.Reset()

	newsRepository := NewNewsRepository(_testDB)

	expected := model.News{
		Author:  _testFaker.Person().Name(),
		Body:    _testFaker.Lorem().Word(),
		Created: time.Now(),
	}
	newsRepository.Save(&expected)

	actual := newsRepository.Find(expected.ID)
	if actual.ID == 0 {
		t.Error("NewsRepository.Find failed to retrieve data")
	}
}
