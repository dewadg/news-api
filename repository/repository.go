package repository

import "github.com/jinzhu/gorm"

// NewNewsRepository creates new NewsRepository instance.
func NewNewsRepository(db *gorm.DB) *NewsRepository {
	return &NewsRepository{
		db: db,
	}
}
