package repository

import "github.com/dewadg/news-api/model"

// NewsRepositoryContract represents interface
// for NewsRepository
type NewsRepositoryContract interface {
	Save(data *model.News) error
	Find(id uint) model.News
}
